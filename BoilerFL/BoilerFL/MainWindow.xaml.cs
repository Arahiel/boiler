﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace BoilerFL
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    // 
    using OxyPlot;
    using OxyPlot.Series;
    using System.ComponentModel;
    using System.Threading;

    public partial class MainWindow : Window
    {
        public PlotModel MyModel { get; private set; }
        private LineSeries plotSeries;
        private Boolean weatherLedStatus = false;
        private Boolean boilerLedStatus = false;
        private Boiler boiler;
        private Weather weather;
        private int plotTime = 0;
        public int timeVal { get; set; } = 0;
        private BackgroundWorker weatherWorker = new BackgroundWorker();
        private BackgroundWorker boilerWorker = new BackgroundWorker();
        private BackgroundWorker chartWorker = new BackgroundWorker();


        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitPlot();
            Loaded += Init;
        }

        private void InitPlot()
        {
            MyModel = new PlotModel { Title = "Temperatura wody" };
            plotSeries = new LineSeries();
            MyModel.Series.Add(plotSeries);
        }

        private void Init(object sender, EventArgs e)
        {
            WeatherON.Fill = new SolidColorBrush(Colors.Gray);
            WeatherOFF.Fill = new SolidColorBrush(Colors.Red);
            BoilerON.Fill = new SolidColorBrush(Colors.Gray);
            BoilerOFF.Fill = new SolidColorBrush(Colors.Red);
            boiler = new Boiler(WaterTempBar, TargetTempSlider, HeatLB);
            weather = new Weather(boiler, WeatherTempBar, WaterTempBar);
            weatherWorker.DoWork += weather.Simulate;
            weatherWorker.WorkerSupportsCancellation = true;
            boilerWorker.DoWork += boiler.Simulate;
            boilerWorker.WorkerSupportsCancellation = true;
            chartWorker.DoWork += ChartUpdate;
            chartWorker.WorkerSupportsCancellation = true;
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }

        private void EDableValues(Boolean t)
        {
            WeatherTempTB.IsEnabled = t;
            WaterTempTB.IsEnabled = t;
            ValuesButton.IsEnabled = t;
        }
        private void WeatherButton_Click(object sender, RoutedEventArgs e)
        {
            weatherLedStatus = !weatherLedStatus;
            if(weatherLedStatus)
            {
                if (!weatherWorker.IsBusy)
                {
                    WeatherButton.Content = "Wyłącz";
                    WeatherON.Fill = new SolidColorBrush(Colors.Green);
                    WeatherOFF.Fill = new SolidColorBrush(Colors.Gray);
                    weather.Status = true;
                    EDableValues(false);
                    weatherWorker.RunWorkerAsync();
                    if (!chartWorker.IsBusy)
                        chartWorker.RunWorkerAsync();
                }
                else
                {
                    weatherLedStatus = false;
                }
            }
            else
            {
                WeatherButton.Content = "Włącz";
                WeatherON.Fill = new SolidColorBrush(Colors.Gray);
                WeatherOFF.Fill = new SolidColorBrush(Colors.Red);
                weather.Status = false;
                if (!boilerLedStatus && !weatherLedStatus)
                {
                    EDableValues(true);
                }
            }
        }

        private void BoilerButton_Click(object sender, RoutedEventArgs e)
        {
            boilerLedStatus = !boilerLedStatus;
            if (boilerLedStatus)
            {
                if (!boilerWorker.IsBusy)
                {
                    BoilerButton.Content = "Wyłącz";
                    BoilerON.Fill = new SolidColorBrush(Colors.Green);
                    BoilerOFF.Fill = new SolidColorBrush(Colors.Gray);
                    boiler.Status = true;
                    EDableValues(false);
                    boilerWorker.RunWorkerAsync();
                    if(!chartWorker.IsBusy)
                        chartWorker.RunWorkerAsync();
                }
                else
                {
                    boilerLedStatus = false;
                }
            }
            else
            {
                BoilerButton.Content = "Włącz";
                BoilerON.Fill = new SolidColorBrush(Colors.Gray);
                BoilerOFF.Fill = new SolidColorBrush(Colors.Red);
                boiler.Status = false;
                if(!boilerLedStatus && !weatherLedStatus)
                {
                    EDableValues(true);
                }
            }
        }

        private void ValuesButton_Click(object sender, RoutedEventArgs e)
        {
            if (!WeatherTempTB.Text.Trim().Equals(""))
                {
                if (int.TryParse(WeatherTempTB.Text, out int wthtemp))
                {
                    weather.WeatherTemperature = wthtemp;
                    weather.UpdateTempBar();
                }
                else
                {
                    MessageBox.Show("Wpisane wartości muszą być w zakresie liczb naturalnych!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WeatherTempTB.Text = "";
                }
            }
            if (!WaterTempTB.Text.Trim().Equals(""))
            {
                if (int.TryParse(WaterTempTB.Text, out int wattemp))
                {
                    boiler.WaterTemperature = wattemp;
                    boiler.UpdateTempBar();
                }
                else
                {
                    MessageBox.Show("Wpisane wartości muszą być w zakresie liczb naturalnych!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WaterTempTB.Text = "";
                }
            }
        }

        private void WaterLevelSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(boiler != null)
            boiler.WaterLevel = (int)WaterLevelSlider.Value;
        }

        private void TimeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (boiler != null && weather != null)
            {
                boiler.timeVal = (int)TimeSlider.Value;
                weather.timeVal = (int)TimeSlider.Value;
            }
        }
        private void ChartUpdate(object sender, DoWorkEventArgs e)
        {
            Int32 time = 1000;
            while(boiler.Status || weather.Status)
            {
                MyModel.Series.Remove(plotSeries);
                time = Int32.Parse((1000 - boiler.timeVal * 100).ToString());
                if(plotSeries.Points.Count >= 100)
                {
                    plotSeries.Points.RemoveAt(0);
                }
                plotSeries.Points.Add(new DataPoint(plotTime,boiler.WaterTemperature));
                plotTime++;
                if(plotTime == 1000000)
                {
                    plotTime = 0;
                    plotSeries = new LineSeries();
                }
                MyModel.Series.Add(plotSeries);
                
                TempPlot.Dispatcher.Invoke(() => {
                    TempPlot.Model = MyModel;
                    TempPlot.InvalidateVisual();
                    TempPlot.InvalidateMeasure();
                    TempPlot.InvalidateArrange();
                    TempPlot.InvalidatePlot();
                });
                Thread.Sleep(time);
            }
        }

    }
}
